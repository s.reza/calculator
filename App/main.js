function Clear(){
    result = document.getElementById("result");
    result.innerText = '0'
}

SetNumber = value =>{
    result = document.getElementById("result");
    if(result.innerText != '0')
        result.innerText += value;
    else
        result.innerText = value;
}

SetOpration = value =>{
    result = document.getElementById("result");
    last = result.innerText.substr(-1);
    if(last == '0' || last == '1' || last == '2' || last == '3' || last == '4' || last == '5' || last == '6' || last == '7' || last == '8' || last == '9'){
        result.innerText += value;
    }
}

function DeleteLast(){
    result = document.getElementById("result");
    if(result.innerText.length != 1){
        last = result.innerText.substring(0, result.innerText.length - 1);
        result.innerText = last;
    }
    else{
        result.innerText = 0;
    }
}

function MakeFloat(){
    result = document.getElementById("result");
    nums = result.innerText.split('+').join(',').split('-').join(',').split('/').join(',').split('*').join(',').split(',');
    Is = nums[nums.length - 1].indexOf('.');
    if(Is == -1)
    result.innerText += '.';
}

function GetResult(){
    result = document.getElementById("result");
    result.innerText = eval(result.innerText);
}